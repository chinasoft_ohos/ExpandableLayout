#  ExpandableLayout

####  项目介绍
- 项目名称：ExpandableLayout
- 所属系列：openharmony的第三方组件适配移植
- 功能：带搜索功能的列表
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.4.1
#### 效果演示 
Expandable LinearLayout

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/151941_84e57435_8950844.gif "case4.gif")

#### 安装教程
方式一：

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:ExpandableLayout:1.0.0')

    ......  

 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
####  使用方式
第一步：
```xml 
  <ScrollView
        ohos:id="$+id:scrollview"
        ohos:background_element="#ffffff"
        ohos:height="match_parent"
        ohos:width="match_parent">

        <DependentLayout
            ohos:orientation="horizontal"
            ohos:height="match_content"
            ohos:width="match_parent">
            <com.hos.expandblelib.ExpandableLayout
                ohos:id="$+id:el"
                ohos:width="match_parent"
                ohos:height="match_parent"/>
        </DependentLayout>
    </ScrollView>
```
第二步：设置数据
```
       sectionLinearLayout.addSection(getSection());
```
第三步：数据更新以及展开关闭的回调
```
   sectionLinearLayout.setRenderer(new ExpandableLayout.Renderer<FruitCategory, Fruit>() {
            @Override
            public void renderParent(Component view, FruitCategory model, boolean isExpanded, int parentPosition) {
                ((Text) view.findComponentById(ResourceTable.Id_tvParent)).setText(model.getName());
                Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
                image.setPixelMap(isExpanded ? ResourceTable.Graphic_arrow_down : ResourceTable.Graphic_arrow_up);
            }

            @Override
            public void renderChild(Component view, Fruit model, int parentPosition, int childPosition) {
                ((Text) view.findComponentById(ResourceTable.Id_tvChild)).setText(model.getName());
            }

        });
```
        sectionLinearLayout.setExpandListener((ExpandCollapseListener.ExpandListener<FruitCategory>) (parentIndex, parent, view) -> {
            Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
            image.setPixelMap(ResourceTable.Graphic_arrow_down);
        });
```
        sectionLinearLayout.setCollapseListener((ExpandCollapseListener.CollapseListener<FruitCategory>) (parentIndex, parent, view) -> {
            Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
            image.setPixelMap(ResourceTable.Graphic_arrow_up);
        });

```
####  测试信息
```
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

```
####  版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT


版权和许可信息
--------

 ```
   Copyright 2017 Mert Şimşek.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 ```



