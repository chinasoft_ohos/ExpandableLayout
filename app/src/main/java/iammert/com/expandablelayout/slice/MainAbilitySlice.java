package iammert.com.expandablelayout.slice;

import iammert.com.expandablelib.ExpandCollapseListener;
import iammert.com.expandablelib.ExpandableLayout;
import iammert.com.expandablelib.Section;
import iammert.com.expandablelayout.Fruit;
import iammert.com.expandablelayout.FruitCategory;
import com.hos.hosexpandablelayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.security.SecureRandom;


public class MainAbilitySlice extends AbilitySlice {

    private ExpandableLayout sectionLinearLayout;
    private TextField editText;

    String[] parents = new String[]{"Fruits",
            "Nice Fruits", "Cool Fruits",
            "Perfect Fruits", "Frozen Fruits",
            "Warm Fruits"};
    private SecureRandom random;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_main);
        editText = (TextField) findComponentById(ResourceTable.Id_edit);
        sectionLinearLayout = (ExpandableLayout) findComponentById(ResourceTable.Id_el);
        sectionLinearLayout.setChildLayout(ResourceTable.Layout_child);
        sectionLinearLayout.setParentLayout(ResourceTable.Layout_parent);
        ScrollView scrollView = (ScrollView) findComponentById(ResourceTable.Id_scrollview);
        scrollView.setAutoLayout(true);
        random = new SecureRandom();
        setData();
    }

    private void setData() {
        sectionLinearLayout.setRenderer(new ExpandableLayout.Renderer<FruitCategory, Fruit>() {
            @Override
            public void renderParent(Component view, FruitCategory model, boolean isExpanded, int parentPosition) {
                ((Text) view.findComponentById(ResourceTable.Id_tvParent)).setText(model.getName());
                Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
                image.setPixelMap(isExpanded ? ResourceTable.Graphic_arrow_down : ResourceTable.Graphic_arrow_up);
            }

            @Override
            public void renderChild(Component view, Fruit model, int parentPosition, int childPosition) {
                ((Text) view.findComponentById(ResourceTable.Id_tvChild)).setText(model.getName());
            }

        });
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.addSection(getSection());
        sectionLinearLayout.setExpandListener((ExpandCollapseListener.ExpandListener<FruitCategory>) (parentIndex, parent, view) -> {
            Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
            image.setPixelMap(ResourceTable.Graphic_arrow_down);
        });
        sectionLinearLayout.setCollapseListener((ExpandCollapseListener.CollapseListener<FruitCategory>) (parentIndex, parent, view) -> {
            Image image = (Image) view.findComponentById(ResourceTable.Id_arrow);
            image.setPixelMap(ResourceTable.Graphic_arrow_up);
        });
        editText.addTextObserver((s, i, i1, i2) -> sectionLinearLayout.filterChildren(obj -> ((Fruit) obj).getName().toLowerCase().contains(s.toString().toLowerCase())));
    }


    public Section<FruitCategory, Fruit> getSection() {
        Section<FruitCategory, Fruit> section = new Section<>();
        int rand = random.nextInt(parents.length);
        FruitCategory fruitCategory = new FruitCategory(parents[rand]);
        Fruit fruit1 = new Fruit("Apple");
        Fruit fruit2 = new Fruit("Orange");
        Fruit fruit3 = new Fruit("Banana");
        Fruit fruit4 = new Fruit("Grape");
        Fruit fruit5 = new Fruit("Strawberry");
        Fruit fruit6 = new Fruit("Cherry");
        section.setParent(fruitCategory);
        section.addChildren(fruit1);
        section.addChildren(fruit2);
        section.addChildren(fruit3);
        section.addChildren(fruit4);
        section.addChildren(fruit5);
        section.addChildren(fruit6);
        section.setExpanded(true);
        return section;
    }


    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
