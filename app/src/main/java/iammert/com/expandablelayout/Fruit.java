package iammert.com.expandablelayout;

/**
 * Created by mertsimsek on 28/07/2017.
 */

public class Fruit {
    private String name;

    public Fruit(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
