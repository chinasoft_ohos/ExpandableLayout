package iammert.com.expandablelayout;

/**
 * Created by mertsimsek on 28/07/2017.
 */

public class FruitCategory {
    private String name;

    public FruitCategory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
