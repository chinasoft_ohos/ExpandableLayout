package iammert.com.expandablelib;

/**
 * Operation
 */
public interface Operation {
    /**
     * apply
     *
     * @param obj Object
     * @return boolean
     */
    boolean apply(Object obj);
}
