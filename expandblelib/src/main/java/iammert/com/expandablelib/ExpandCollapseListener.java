package iammert.com.expandablelib;


import ohos.agp.components.Component;

/**
 * Created by mertsimsek on 28/07/2017.
 */
public interface ExpandCollapseListener<P> {
    interface ExpandListener<P> {
        void onExpanded(int parentIndex, P parent, Component view);
    }

    interface CollapseListener<P> {
        void onCollapsed(int parentIndex, P parent, Component view);
    }
}
