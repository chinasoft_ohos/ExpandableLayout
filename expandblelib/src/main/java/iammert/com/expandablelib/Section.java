package iammert.com.expandablelib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertsimsek on 28/07/2017.
 */
public class Section<P, C> {
    private boolean expanded;
    private P parent;
    private List<C> children;

    public Section() {
        children = new ArrayList<>();
        expanded = false;
    }

    /**
     * isExpanded
     *
     * @return boolean
     */
    public boolean isExpanded() {
        return expanded;
    }

    /**
     * setExpanded
     *
     * @param expanded expanded
     */
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /**
     * getParent
     *
     * @return P
     */
    public P getParent() {
        return parent;
    }

    /**
     * setParent
     *
     * @param parent P
     */
    public void setParent(P parent) {
        this.parent = parent;
    }

    /**
     * getChildren
     *
     * @return List<C>
     */
    public List<C> getChildren() {
        return children;
    }

    /**
     * setChildren
     *
     * @param children List<C>
     */
    public void setChildren(List<C> children) {
        this.children = children;
    }

    /**
     * addChildren
     *
     * @param children C
     */
    public void addChildren(C children) {
        this.children.add(children);
    }
}
